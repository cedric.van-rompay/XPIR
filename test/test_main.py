# coding: utf-8
import subprocess
import os
import shutil
import logging

DB_FILENAME = '4792.zgbf.bin'

logging.basicConfig(level=logging.INFO)

build_abspath = os.path.abspath('./_build/')


shutil.rmtree('/tmp/pir-test', ignore_errors=True)
os.mkdir('/tmp/pir-test')

logging.info("Creating directories")
for i in [1,2,3]:
    test_subdir = '/tmp/pir-test/{}'.format(i)
    os.mkdir(test_subdir)
    shutil.copytree('exp', test_subdir+'/exp')
    os.mkdir(test_subdir + '/' + 'reception')
    os.mkdir(test_subdir + '/' + 'db')
    shutil.copy('data/'+DB_FILENAME, test_subdir + '/db/')

logging.info("starting servers")
server_processes = list()
for i in [1,2,3]:
    test_subdir = '/tmp/pir-test/{}'.format(i)
    server_logfile_stdout = open(test_subdir + '/server.stdout.log', 'w')
    server_logfile_stderr = open(test_subdir + '/server.stderr.log', 'w')
    port = 3000 + i
    server_processes.append(
        subprocess.Popen(
            [build_abspath + '/apps/server/pir_server', '-p', str(port), '-s', '4792'],
            cwd=test_subdir,
            stdout = server_logfile_stdout,
            stderr = server_logfile_stderr 
        )
    )

logging.info("starting clients")
client_processes = list()
for i in [1,2,3]:
    test_subdir = '/tmp/pir-test/{}'.format(i)
    client_logfile_stdout = open(test_subdir + '/client.stdout.log', 'w')
    client_logfile_stderr = open(test_subdir + '/client.stderr.log', 'w')
    port = 3000 + i
    client_processes.append(
        subprocess.Popen(
            (build_abspath + '/apps/client/pir_client -c -r LWE.* --reclvl 2 --alpha 1 --port {} --no-pipeline'.format(port)).split(),
            cwd=test_subdir,
            stdout = client_logfile_stdout,
            stderr=client_logfile_stderr
        )
    )

for client_process in client_processes:
    client_process.wait()
    if client_process.returncode != 0:
        logging.warning('client process terminated with non-zero return code: {}'.format(client_process))

for server_process in server_processes:
    server_process.kill()
    server_process.wait()

for i in [1,2,3]:
    test_subdir = '/tmp/pir-test/{}'.format(i)
    print("test result for",i)
    with open(test_subdir + '/reception' + '/0', 'rb') as f:
        retrieved_share = f.read()
        
    with open(test_subdir + '/db/'+DB_FILENAME, 'rb') as f:
        target_share = f.read(8)
        
    if retrieved_share == target_share:
        print('OK')
    else:
        print('ERROR')
